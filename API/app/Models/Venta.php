<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory;
    // Nombre de la tabla en MySQL.
    protected $table = 'ventas';

    // Eloquent asume que cada tabla tiene una clave primaria con una columna llamada id.
    // Si éste no fuera el caso entonces hay que indicar cuál es nuestra clave primaria en la tabla:
    //protected $primaryKey = 'id_venta';
    
    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = array('id_venta','nombre_articulo','descripcion','cantidad','precio','id_usuario','id_cliente','id_articulo');
}
