<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    // Nombre de la tabla en MySQL.
    protected $table = 'clientes';

    // Eloquent asume que cada tabla tiene una clave primaria con una columna llamada id.
    // Si éste no fuera el caso entonces hay que indicar cuál es nuestra clave primaria en la tabla:
    protected $primaryKey = 'id_cliente';
    
    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = array('nombre_cliente','direcion_cliente','email_cliente','telefono_cliente','ci_cliente','id_usuario');
    
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at','updated_at'];
}
