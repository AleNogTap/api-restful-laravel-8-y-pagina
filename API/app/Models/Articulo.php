<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articulo extends Model
{
    use HasFactory;
    // Nombre de la tabla en MySQL.
    protected $table = 'articulos';

    // Eloquent asume que cada tabla tiene una clave primaria con una columna llamada id.
    // Si éste no fuera el caso entonces hay que indicar cuál es nuestra clave primaria en la tabla:
    protected $primaryKey = 'id_articulo';
    
    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = array('nombre_articulo','descripcion','cantidad','precio','id_usuario','id_categoria');
    
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at','updated_at'];
}
