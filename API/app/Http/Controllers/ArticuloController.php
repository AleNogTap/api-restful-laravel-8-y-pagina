<?php

namespace App\Http\Controllers;

use App\Models\Articulo;
use App\Resources\ArticuloResource;
use Illuminate\Http\Request;

class ArticuloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articulo = Articulo::all();
        return response()->json($articulo, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $articulo = new Articulo($request->all());
        $articulo->save();
        return response()->json($articulo, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Articulo  $articulo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $articulo = Articulo::findOrFail($id);
        return response()->json($articulo, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Articulo  $articulo
     * @return \Illuminate\Http\Response
     */
    public function edit(Articulo $articulo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Articulo  $articulo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $articulo = Articulo::findOrFail($id);
        $articulo -> nombre_articulo=$request->nombre_articulo;
        $articulo -> descripcion=$request->descripcion;
        $articulo -> cantidad=$request->cantidad;
        $articulo -> precio=$request->precio;
        $articulo -> id_usuario=$request->id_usuario;
        $articulo -> id_categoria=$request->id_categoria;
        $result = $articulo->save();
        if($result)
        {
            return response()->json($articulo, 200);
        }
        else
        {
            return ["result"=>"No se pudo Modificar"];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Articulo  $articulo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $articulo = Articulo::destroy($id);
        return $articulo;
    }
}
