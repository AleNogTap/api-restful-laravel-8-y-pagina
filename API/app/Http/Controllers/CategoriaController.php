<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use App\Resources\CategoriaResource;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoria = Categoria::all();
        return response()->json($categoria, 200);
        //return new CategoriaResource($categoria);
        //return response([ 'categorias' => CategoriaResource::collection($categoria), 'message' => 'successfully'], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $categoria = new Categoria();
        $categoria -> nombre_categoria=$request->$nombre_categoria;
        $categoria -> id_usuario=$request->$id_usuario;
        if($categoria->save())
        {
            return new CategoriaResource($categoria);
        }
        */
        $categoria = new Categoria($request->all());
        $categoria->save();
        return response()->json($categoria, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*
        $categoria = Categoria::findOrFail($id);
        return new CategoriaResource($categoria);
        */
        $categoria = Categoria::findOrFail($id);
        return response()->json($categoria, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        $categoria = Categoria::findOrFail($id);
        $categoria -> nombre_categoria=$request->$nombre_categoria;
        $categoria -> id_usuario=$request->$id_usuario;
        if($categoria->save())
        {
            return new CategoriaResource($categoria);
        }
        */
        $categoria = Categoria::findOrFail($id);
        $categoria -> nombre_categoria=$request->nombre_categoria;
        $categoria -> id_usuario=$request->id_usuario;
        $result = $categoria->save();
        if($result)
        {
            return response()->json($categoria, 200);
        }
        else
        {
            return ["result"=>"No se pudo Modificar"];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        $categoria = Categoria::findOrFail($id);
        if($categoria->delete())
        {
            return new CategoriaResource($categoria);
        }
        */
        $categoria = Categoria::destroy($id);
        return $categoria;
    }
}
