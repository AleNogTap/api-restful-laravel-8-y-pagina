<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Resources\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return response()->json($users, 200);
        //return UserResource::collection($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        $users = new User();
        $users -> nombre=$request->$nombre;
        $users -> email=$request->$email;
        $users -> contraseña=$request->$contraseña;
        if($users->save())
        {
            //return new UserResource($users);
            return response()->json($users, 200);
        }*/
        $users = new User($request->all());
        $users->save();
        return response()->json($users, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::findOrFail($id);
        return response()->json($users, 200);
        //return new UserResource($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        $users = User::findOrFail($id);
        $users -> nombre=$request->$nombre;
        $users -> email=$request->$email;
        $users -> contraseña=$request->$contraseña;
        if($users->save())
        {
            return new UserResource($users);
        }*/

        $users = User::findOrFail($id);
        $users -> nombre= $request ->nombre;
        $users -> email= $request->email;
        $result = $users->save();
        if($result)
        {
            return response()->json($users, 200);
        }
        else
        {
            return ["result"=>"No se pudo Modificar"];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /*
        $users = User::findOrFail($id);
        if($users->delete())
        {
            return new UserResource($users);
        }
        */
        $users = User::destroy($id);
        return $users;
    }
}
