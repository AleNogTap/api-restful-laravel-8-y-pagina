<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Articulo;
use Faker\Factory as Faker;
class ArticuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        //numero de usuarios y categorias en la BD
        $cuantos= User::all()->count();
        $cuantas= Categoria::all()->count();
		// Creamos un bucle para cubrir 10 articulos:
		for ($i=0; $i<9; $i++)
		{
			// Cuando llamamos al método create del Modelo Categorias 
			// se está creando una nueva fila en la tabla.
			Articulo::create(
				[
					'nombre_articulo'=>$faker->word(),
                    'descripcion'=>$faker->word(),
                    'cantidad'=>$faker->randomNumber(3),
                    'precio'=>$faker->randomFloat(2,10),
                    'id_usuario'=>$faker->numberBetween(1,$cuantos),
					'id_categoria'=>$faker->numberBetween(1,$cuantas)
				]
			);
		}
    }
}
