<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        /**$this->call('CategoriaSeeder');
        $this->call('ArticuloSeeder');
        $this->call('ClienteSeeder');*/
        \App\Models\Categoria::factory(10)->create();
    }
}
