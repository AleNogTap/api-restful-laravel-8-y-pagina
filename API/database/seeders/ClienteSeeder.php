<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Cliente;
use Faker\Factory as Faker;
class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        //numero de usuarios en la BD
        $cuantos= User::all()->count();
		// Creamos un bucle para cubrir 10 clientes:
		for ($i=0; $i<9; $i++)
		{
			// Cuando llamamos al método create del Modelo Categorias 
			// se está creando una nueva fila en la tabla.
			Categoria::create(
				[
					'nombre_cliente'=>$faker->word(),
                    'direcion_cliente'=>$faker->word(),
                    'email_cliente'=>$faker->word(),
                    'telefono_cliente'=>$faker->randomNumber(8),
                    'ci_cliente'=>$faker->randomNumber(10),
                    'id_usuario'=>$faker->numberBetween(1,$cuantos)
				]
			);
		}
    }
}
