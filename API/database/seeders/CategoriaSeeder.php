<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Categoria;
use Faker\Factory as Faker;
class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        //numero de usuarios en la BD
        $cuantos= User::all()->count();
		// Creamos un bucle para cubrir 5 categorias:
		for ($i=0; $i<4; $i++)
		{
			// Cuando llamamos al método create del Modelo Categorias 
			// se está creando una nueva fila en la tabla.
			Categoria::create(
				[
					'nombre_categoria'=>$faker->word(15),
					'id_usuario'=>$faker->numberBetween(1,$cuantos)
				]
			);
		}
    }
}
