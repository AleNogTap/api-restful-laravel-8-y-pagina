<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ArticuloController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\VentaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//Si se utiliza Resources
//Route::resource('users', 'UserController', ['except' => ['create', 'edit']]);
//Route::resource('categorias', 'CategoriaController', ['except' => ['create', 'edit']]);
//dos maneras de declarar rutas
Route::get('/User', 'UserController@index')->name('index');
Route::post('/User', 'UserController@store')->name('store');
Route::get('/User/{id_usuario}', 'UserController@show')->name('show');
Route::put('/User/{id_usuario}', 'UserController@update')->name('update');
Route::delete('/User/{id_usuario}', 'UserController@destroy')->name('destroy');
//Route::get("/User",[UserController::class,'index']);
//Route::post("/User",[UserController::class,'store']);
//Route::get("/User/{id_usuario}",[UserController::class,'show']);
//Route::put("/User/{id_usuario}",[UserController::class,'update']);
//Route::delete("/User/{id_usuario}",[UserController::class,'destroy']);

Route::get('/Categoria', 'CategoriaController@index')->name('index');
Route::post('/Categoria', 'CategoriaController@store')->name('store');
Route::get('/Categoria/{id_categoria}', 'CategoriaController@show')->name('show');
Route::put('/Categoria/{id_categoria}', 'CategoriaController@update')->name('update');
Route::delete('/Categoria/{id_categoria}', 'CategoriaController@destroy')->name('destroy');
Route::get("/Categoria",[CategoriaController::class,'index']);
/*Route::get('Categoria', 'Api\CategoriaController@index');
Route::get('/Categoria', [CategoriaController::class, 'index']);
Route::post("/Categoria",[CategoriaController::class,'store']);
Route::get("/Categoria/{id_categoria}",[CategoriaController::class,'show']);
Route::put("/Categoria/{id_categoria}",[CategoriaController::class,'update']);
Route::delete("/Categoria/{id_categoria}",[CategoriaController::class,'destroy']);*/

Route::get('/Articulo', 'ArticuloController@index')->name('index');
Route::post('/Articulo', 'ArticuloController@store')->name('store');
Route::get('/Articulo/{id_articulo}', 'ArticuloController@show')->name('show');
Route::put('/Articulo/{id_articulo}', 'ArticuloController@update')->name('update');
Route::delete('/Articulo/{id_articulo}', 'ArticuloController@destroy')->name('destroy');

Route::get('/Cliente', 'ClienteController@index')->name('index');
Route::post('/Cliente', 'ClienteController@store')->name('store');
Route::get('/Cliente/{id_cliente}', 'ClienteController@show')->name('show');
Route::put('/Cliente/{id_cliente}', 'ClienteController@update')->name('update');
Route::delete('/Cliente/{id_cliente}', 'ClienteController@destroy')->name('destroy');

Route::get('/Venta', 'VentaController@index')->name('index');
Route::post('/Venta', 'VentaController@store')->name('store');
Route::get('/Venta/{id_venta}', 'VentaController@show')->name('show');
Route::put('/Venta/{id_venta}', 'VentaController@update')->name('update');
Route::delete('/Venta/{id_venta}', 'VentaController@destroy')->name('destroy');