<?php require_once "API.php"; ?>
<?php 
/**Con esta clase usamos el API con la clase llamada antes API.php */
$URL='http://127.0.0.1:8000/api/';
	class clientes{
		public function agregaCliente($datos){
			$parametros = array(
				'id_usuario' 	=> '$datos[0]',
				'nombre'	=> '$datos[1]',
				'apellido'	=> '$datos[2]',
				'direccion' 	=> '$datos[3]',
				'email' 	=> '$datos[4]',
				'telefono' 	=> '$datos[5]',
				'ci' 	=> '$datos[6]'
			);
			$rs = API::POST($URL.'Cliente',$parametros);
			$rs = API::JSON_TO_ARRAY($rs);
			return $rs;
		}

		public function obtenDatosCliente($idcliente){
			$rs 	= API::GET($URL.'Cliente/'.$idcliente);
			$array  = API::JSON_TO_ARRAY($rs);
			return $array;
		}

		public function actualizaCliente($datos){
			$parametros = array(
				'id_usuario' 	=> '$datos[1]',
				'nombre'	=> '$datos[2]',
				'apellido'	=> '$datos[3]',
				'direccion' 	=> '$datos[4]',
				'email' 	=> '$datos[5]',
				'telefono' 	=> '$datos[6]',
				'ci' 	=> '$datos[7]'
			);
			$rs = API::PUT($URL.'Cliente/'.$datos[0],$parametros);
			$rs = API::JSON_TO_ARRAY($rs);
			return $rs;
		}

		public function eliminaCliente($idcliente){
			$rs 	= API::DELETE($URL.'Cliente/'.$datos[0]);
			$rs 	= API::JSON_TO_ARRAY($rs);
			$result=$rs;
		}
	}
 ?>