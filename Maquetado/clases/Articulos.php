<?php require_once "clases\API.php"; ?>
<?php 
/**Con esta clase usamos el API con la clase llamada antes API.php */
$URL='http://127.0.0.1:8000/api/';

	class articulos{
		public function insertaArticulo($datos){
			$parametros = array(
				'id_categoria' 	=> '$datos[0]',
				'id_imagen'	=> '$datos[1]',
				'id_usuario'	=> '$datos[2]',
				'nombre' 	=> '$datos[3]',
				'descripcion' 	=> '$datos[4]',
				'cantidad' 	=> '$datos[5]',
				'precio' 	=> '$datos[6]'
			);
			$rs = API::POST($URL.'Articulo',$parametros);
			$rs = API::JSON_TO_ARRAY($rs);
			return $rs;
		}

		public function obtenDatosArticulo($idarticulo){
			$rs 	= API::GET($URL.'Articulo/'.$idarticulo);
			$array  = API::JSON_TO_ARRAY($rs);
			return $array;
		}

		public function actualizaArticulo($datos){
			$parametros = array(
				'id_categoria' 	=> '$datos[1]',
				'id_imagen'	=> '$datos[2]',
				'id_usuario'	=> '$datos[3]',
				'nombre' 	=> '$datos[4]',
				'descripcion' 	=> '$datos[5]',
				'cantidad' 	=> '$datos[6]',
				'precio' 	=> '$datos[7]'
			);
			$rs = API::PUT($URL.'Articulo/'.$datos[0],$parametros);
			$rs = API::JSON_TO_ARRAY($rs);
			return $rs;
		}

		public function eliminaArticulo($idarticulo){
			$rs 	= API::DELETE($URL.'Articulo/'.$datos[0]);
			$rs 	= API::JSON_TO_ARRAY($rs);
			$result=$rs;
		}
	}
 ?>