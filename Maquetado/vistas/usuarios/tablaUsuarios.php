<?php
	$datos=json_decode(file_get_contents("http://127.0.0.1:8000/api/User"),true);
?>

<table class="table table-hover table-condensed table-bordered" style="text-align: center;">
	<caption><label>Usuarios :)</label></caption>
	<tr>
		<td>Nombre</td>
		<td>Apellido</td>
		<td>Editar</td>
		<td>Eliminar</td>
	</tr>
	<?php
        for	($i=0; $i<count($datos);$i++){
    ?>
	<tr>
		<td><?php echo($datos[$i]["nombre"]); ?></td>
		<td><?php echo($datos[$i]["email"]); ?></td>
		<td>
			<span data-toggle="modal" data-target="#actualizaUsuarioModal" class="btn btn-warning btn-xs" onclick="agregaDatosUsuario('<?php echo $ver[0]; ?>')">
				<span class="glyphicon glyphicon-pencil"></span>
			</span>
		</td>
		<td>
			<span class="btn btn-danger btn-xs" onclick="eliminarUsuario('<?php echo $ver[0]; ?>')">
				<span class="glyphicon glyphicon-remove"></span>
			</span>
		</td>
	</tr>
	<?php
        }
    ?>  
</table>