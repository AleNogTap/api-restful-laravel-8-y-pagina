<?php
	$datos=json_decode(file_get_contents("http://127.0.0.1:8000/api/Articulo/"),true);
?>

<table class="table table-hover table-condensed table-bordered" style="text-align: center;">
	<caption><label>Articulos</label></caption>
	<tr>
		<td>Nombre</td>
		<td>Descripcion</td>
		<td>Cantidad</td>
		<td>Precio</td>
		<td>Categoria</td>
		<td>Editar</td>
		<td>Eliminar</td>
	</tr>

	<?php
        for	($i=0; $i<count($datos);$i++){
    ?>
	<tr>
		<td><?php echo($datos[$i]["nombre_articulo"]); ?></td>
		<td><?php echo($datos[$i]["descripcion"]); ?></td>
		<td><?php echo($datos[$i]["cantidad"]); ?></td>
		<td><?php echo($datos[$i]["precio"]); ?></td>
		<td><?php echo($datos[$i]["id_categoria"]); ?></td>
		<td>
			<?php 
			$imgVer=explode("/", $ver[4]) ; 
			$imgruta=$imgVer[1]."/".$imgVer[2]."/".$imgVer[3];
			?>
			<img width="80" height="80" src="<?php echo $imgruta ?>">
		</td>
		<td><?php echo $ver[5]; ?></td>
		<td>
			<span  data-toggle="modal" data-target="#abremodalUpdateArticulo" class="btn btn-warning btn-xs" onclick="agregaDatosArticulo('<?php echo $ver[6] ?>')">
				<span class="glyphicon glyphicon-pencil"></span>
			</span>
		</td>
		<td>
			<span class="btn btn-danger btn-xs" onclick="eliminaArticulo('<?php echo $ver[6] ?>')">
				<span class="glyphicon glyphicon-remove"></span>
			</span>
		</td>
	</tr>
<?php } ?>
</table>