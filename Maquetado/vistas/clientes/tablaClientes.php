<?php
	$datos=json_decode(file_get_contents("http://127.0.0.1:8000/api/Cliente"),true);
?>

<div class="table-responsive">
	 <table class="table table-hover table-condensed table-bordered" style="text-align: center;">
	 	<caption><label>Clientes :)</label></caption>
	 	<tr>
	 		<td>Nombre</td>
	 		<td>Direccion</td>
	 		<td>Email</td>
	 		<td>Telefono</td>
	 		<td>NIT</td>
	 		<td>Editar</td>
	 		<td>Eliminar</td>
	 	</tr>
		 <?php
        for	($i=0; $i<count($datos);$i++){
    	?>
	 	<tr>
	 		<td><?php echo($datos[$i]["nombre_cliente"]); ?></td>
	 		<td><?php echo($datos[$i]["direcion_cliente"]); ?></td>
			<td><?php echo($datos[$i]["email_cliente"]); ?></td>
			<td><?php echo($datos[$i]["telefono_cliente"]); ?></td>
	 		<td><?php echo($datos[$i]["ci_cliente"]); ?></td>
	 		<td>
				<span class="btn btn-warning btn-xs" data-toggle="modal" data-target="#abremodalClientesUpdate" onclick="agregaDatosCliente('<?php echo $ver[0]; ?>')">
					<span class="glyphicon glyphicon-pencil"></span>
				</span>
			</td>
			<td>
				<span class="btn btn-danger btn-xs" onclick="eliminarCliente('<?php echo $ver[0]; ?>')">
					<span class="glyphicon glyphicon-remove"></span>
				</span>
			</td>
	 	</tr>
	 <?php } ?>
	 </table>