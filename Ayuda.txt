Para crear la base de datos, se debe utilizar el comando "php artisan migrate", la version de php debe ser mayor a la 7.3.x
el cual nos genera la migracion de la base de datos segun el modelo presentado en las carpetas de Models y en Migrate,
para correr esta API se necesita instalar LARAVEL 8.xx asi como Composer2.x y una version mayor a la 12.14 de Node.js,

Los controladores y los resources se encuentran en la carpeta app/Http/, al igual que los Models, en database/ podemos encontrar:
factories, migrations y seeders.
Finalmente en routes/api se pusieron todas las rutas de nnuestros controladores

Para ejecutar el servicio se utiliza el comando "php artisan serve", el cual nos habilita un puerto del localhost.


